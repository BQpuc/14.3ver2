﻿#include <iostream> 
#include <iomanip> 
#include <string>  

int main()
{
	// записываем переменную типа string, пишем в консоль 	
	std::string name;
	std::cout << "Enter your name: " << "\n";
	//std::cin >> name;

	// инициализируем переменную путем ввода в консоль. Переменная включает пробелы. 	
	std::getline(std::cin, name);

	// Выводим в консоль переменную и количесво символов 	
	std::cout << "Name: " << name << "\n" << "Lenght: " << name.length() << "\n";

	// Выводим первый и последний символ 	
	std::cout << "First: " << name[0] << "\n";
	std::cout << "Last: " << name[name.length() - 1] << "\n";

	return 0;
}